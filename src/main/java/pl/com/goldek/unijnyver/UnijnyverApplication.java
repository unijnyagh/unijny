package pl.com.goldek.unijnyver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class UnijnyverApplication implements WebMvcConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(UnijnyverApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(UnijnyverApplication.class, args);
        logger.info("Zaczęło działąć");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("/webjars/");

    }

}
