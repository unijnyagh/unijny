package pl.com.goldek.unijnyver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.com.goldek.unijnyver.model.Uzytkownik;
import pl.com.goldek.unijnyver.service.UzytkownikService;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GlobalController {

    @Autowired
    private UzytkownikService uzytkownikService;

    private Uzytkownik loginUzytkownik;

    public Uzytkownik getLoginUzytkownik() {
        if (loginUzytkownik == null) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            loginUzytkownik = uzytkownikService.findByEmail(auth.getName());
        }
        return loginUzytkownik;
    }
}
