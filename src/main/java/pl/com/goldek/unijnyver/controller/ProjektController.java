package pl.com.goldek.unijnyver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.com.goldek.unijnyver.model.Projekt;
import pl.com.goldek.unijnyver.model.WartoscWskaznika;
import pl.com.goldek.unijnyver.service.ProjektService;
import pl.com.goldek.unijnyver.service.StatusProjektuService;
import pl.com.goldek.unijnyver.service.WartoscWskaznikaService;
import pl.com.goldek.unijnyver.service.WskaznikService;
import pl.com.goldek.unijnyver.utils.WartoscWskaznikSetWrapper;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Controller
public class ProjektController {

    private static final Logger logger = LoggerFactory.getLogger(ProjektController.class);

    @Autowired
    GlobalController globalController;

    @Autowired
    ProjektService projektService;

    @Autowired
    StatusProjektuService statusProjektuService;

    @Autowired
    WskaznikService wskaznikService;

    @Autowired
    WartoscWskaznikaService wartoscWskaznikaService;


    @RequestMapping(value = {"/projekty/"}, method = RequestMethod.POST)
    public String zapiszProjekt(@ModelAttribute("reqProjekt") Projekt reqProjekt, final RedirectAttributes redirectAttributes, @RequestParam String iloscWsk, @RequestParam Map<String, String> allParams) {
        logger.info("Będziemy zapisywać /projekty/");
//        try {
//            reqProjekt.setStatusProjektu(statusProjektuService.findById(2));
//            reqProjekt.setUzytkownik(globalController.getLoginUzytkownik());
//            reqProjekt.setDataZgloszenia(LocalDateTime.now());
//
//            int liczbaWsk = Integer.valueOf(iloscWsk);
//            logger.info("wszysktie parametry" + allParams.toString());
//            Set<WartoscWskaznika> wartosci = new HashSet<>();
//            for (int i = 0; i < liczbaWsk; i++) {
//                int idWskaznika = Integer.valueOf(allParams.get("wskaznik" + i));
//
//                wartosci.add(new WartoscWskaznika(wskaznikService.findById(idWskaznika)
//                        , reqProjekt, Double.valueOf(allParams.get("iloscWskaznik" + i))));
////                wartosci.add(wartoscWskaznika);
////                wartosci.add(new WartoscWskaznika(wskaznikService.findById(idWskaznika)
////                        , reqProjekt, Double.valueOf(allParams.get("iloscWskaznik" + i))));
//            }
//            System.out.println("!!!!!!!!!!!!!!! ZAPISAŁEM WSKAŹNIKI I JEST ICH " + wartosci.size());
//            reqProjekt.setWartosciWskaznikowProjektu(wartosci);
//            projektService.save(reqProjekt);
////            System.out.println("Projekt ma id: " + reqProjekt.getId());
////            for (WartoscWskaznika w : reqProjekt.getWartosciWskaznikowProjektu()){
////                System.out.println("WARTOść wskażnika " + w);
////            }
//            wartoscWskaznikaService.saveCollection(wartosci);
//            redirectAttributes.addFlashAttribute("msg", "success");
//
//        } catch (Exception e) {
//            redirectAttributes.addFlashAttribute("msg", "fail");
//            logger.error("save projekt " + e.getMessage());
//        }
//        return "redirect:/home";
//    }
        try{
            reqProjekt.setStatusProjektu(statusProjektuService.findById(2));
            reqProjekt.setUzytkownik(globalController.getLoginUzytkownik());

            int liczbaWsk = Integer.valueOf(iloscWsk);
            logger.info("wszysktie parametry" + allParams.toString());
            Set<WartoscWskaznika> wartosci = new HashSet<>();
            for (int i = 0; i < liczbaWsk; i++){
                int idWskaznika = Integer.valueOf(allParams.get("wskaznik" + i));

                wartosci.add(new WartoscWskaznika(wskaznikService.findById(idWskaznika)
                        , reqProjekt, Double.valueOf(allParams.get("iloscWskaznik" + i))));
            }
            reqProjekt.setWartosciWskaznikowProjektu(wartosci);
            projektService.save(reqProjekt);
            wartoscWskaznikaService.saveCollection(wartosci);
            redirectAttributes.addFlashAttribute("msg", "success");

        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("msg", "fail");
            logger.error("save projekt " + e.getMessage());
        }
        return "redirect:/home";
    }


}
