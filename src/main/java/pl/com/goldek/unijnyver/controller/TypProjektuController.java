package pl.com.goldek.unijnyver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.com.goldek.unijnyver.model.TypProjektu;
import pl.com.goldek.unijnyver.model.Wskaznik;
import pl.com.goldek.unijnyver.service.TypProjektuService;
import pl.com.goldek.unijnyver.service.WskaznikService;

import java.util.Set;

@Controller
public class TypProjektuController {

    private static Logger logger = LoggerFactory.getLogger(TypProjektuController.class);

    @Autowired
    GlobalController globalController;

    @Autowired
    WskaznikService wskaznikService;

    @Autowired
    TypProjektuService typProjektuService;

    @RequestMapping(value = "/typ_projektu/{id}/wskazniki", method = RequestMethod.GET)
    public @ResponseBody
    Set<Wskaznik> wskaznikiDlaTypuProjektu(
            @PathVariable("id") int idTypProjektu) {
        TypProjektu typProjektu = typProjektuService.findById(idTypProjektu);
        logger.info("Szukam wskaźników dla typu projetku: " + typProjektu);
        return typProjektu.getWskazniki();
    }
}
