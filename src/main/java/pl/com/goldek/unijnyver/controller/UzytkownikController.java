package pl.com.goldek.unijnyver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.com.goldek.unijnyver.model.Projekt;
import pl.com.goldek.unijnyver.model.Uzytkownik;
import pl.com.goldek.unijnyver.service.ProjektService;
import pl.com.goldek.unijnyver.service.StatusProjektuService;
import pl.com.goldek.unijnyver.service.TypProjektuService;
import pl.com.goldek.unijnyver.service.UzytkownikService;
import pl.com.goldek.unijnyver.utils.PassEncoding;
import pl.com.goldek.unijnyver.utils.Role;
import pl.com.goldek.unijnyver.utils.WartoscWskaznikSetWrapper;

import java.time.LocalDateTime;

//@Controller
//public class UzytkownikController {
//
//    private static final Logger logger = LoggerFactory.getLogger(UzytkownikController.class);
//
//    @Autowired
//    GlobalController globalController;
//
//    @Autowired
//    UzytkownikService uzytkownikService;
//
//    @RequestMapping("/")
//    public String root(Model model) {
//        model.addAttribute("reqUzytkownik", new Uzytkownik());
//        logger.info("root");
//        return "login";
//    }
//
//    @RequestMapping("/login.html")
//    public String home(Model model) {
//        model.addAttribute("reqUzytkownik", new Uzytkownik());
//        logger.info("login");
//        return "login";
//    }
//
//    @RequestMapping("/register")
//    public String register(Model model) {
//        model.addAttribute("reqUzytkownik", new Uzytkownik());
//        logger.info("register");
//        return "register";
//    }
//
//    @RequestMapping("/admin")
//    public String helloAdmin() {
//        logger.info("admin");
//        return "admin";
//    }
//
//    @RequestMapping(value = {"user/register"},method = RequestMethod.POST)
//    public String register(@ModelAttribute("reqUzytkownik") Uzytkownik reqUzytkownik,
//                           final RedirectAttributes redirectAttributes){
//        logger.info("user/register ");
//        Uzytkownik uzytkownik = uzytkownikService.findByEmail(reqUzytkownik.getEmail());
//        if (uzytkownik != null) {
//            redirectAttributes.addFlashAttribute("saveUzytkownik", "exist-email");
//            return "redirect:/register";
//        }
//        reqUzytkownik.setHaslo(PassEncoding.getInstance().passwordEncoder.encode(reqUzytkownik.getHaslo()));
//        reqUzytkownik.setRola(Role.ROLE_USER.getValue());
//        logger.info("oto user: "+reqUzytkownik);
//        if (uzytkownikService.save(reqUzytkownik) != null ) {
//            redirectAttributes.addFlashAttribute("saveUzytkownik", "succes");
//        } else {
//            redirectAttributes.addFlashAttribute("saveUzytkownik", "fail");
//        }
//
//        return "redirect:/register";
//    }
//
//
//}
@Controller
public class UzytkownikController {

    private static final Logger logger = LoggerFactory.getLogger(UzytkownikController.class);

    @Autowired
    GlobalController globalController;

    @Autowired
    UzytkownikService userService;

    @Autowired
    TypProjektuService typProjektuService;

    @Autowired
    StatusProjektuService statusProjektuService;

    @Autowired
    ProjektService projektService;

    @RequestMapping("/")
    public String root(Model model) {
        model.addAttribute("reqUzytkownik", new Uzytkownik());
        logger.info("root");
        return "login";
    }

    @RequestMapping("/login")
    public String login(Model model) {
        model.addAttribute("reqUzytkownik", new Uzytkownik());
        logger.info("login" + model.toString());
        return "login";
    }

    @RequestMapping("/home")
    public String home(Model model) {
        Uzytkownik uzytkownik = globalController.getLoginUzytkownik();
        model.addAttribute("reqProjekt", new Projekt());
        model.addAttribute("wszystkieTypyProjektow", typProjektuService.findAll());
        model.addAttribute("wszystkieStatusy", statusProjektuService.findAll());
        model.addAttribute("listaProjektowUzytkownika", projektService.findAllByUzytkownik(uzytkownik));
        model.addAttribute("wartosciWskaznikow", new WartoscWskaznikSetWrapper());
        logger.info("home");
        return "home";
    }

    @RequestMapping("/admin")
    public String admin(Model model) {
        model.addAttribute("listaUzytkownikow", userService.findAll());
        model.addAttribute("listaProjektow", projektService.findAll());
        return "/admin";
    }

    @RequestMapping("/register")
    public String register(Model model) {
        model.addAttribute("reqUzytkownik", new Uzytkownik());
        logger.info("register");
        return "register";
    }

    @RequestMapping(value = {"/user/register"}, method = RequestMethod.POST)
    public String register(@ModelAttribute("reqUzytkownik") Uzytkownik reqUzytkownik,
                           final RedirectAttributes redirectAttributes) {

        logger.info("/user/register");
        Uzytkownik user = userService.findByEmail(reqUzytkownik.getEmail());
        if (user != null) {
            redirectAttributes.addFlashAttribute("saveUzytkownik", "exist-email");
            return "redirect:/register";
        }

        reqUzytkownik.setHaslo(PassEncoding.getInstance().passwordEncoder.encode(reqUzytkownik.getHaslo()));
        reqUzytkownik.setRola(Role.ROLE_USER.getValue());
        reqUzytkownik.setDataRejestracji(LocalDateTime.now());
        reqUzytkownik.setCzyAktywne(true);

        if (userService.save(reqUzytkownik) != null) {
            redirectAttributes.addFlashAttribute("saveUzytkownik", "success");
        } else {
            redirectAttributes.addFlashAttribute("saveUzytkownik", "fail");
        }

        return "redirect:/register";
    }


}
