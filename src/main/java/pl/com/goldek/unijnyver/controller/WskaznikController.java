package pl.com.goldek.unijnyver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.com.goldek.unijnyver.model.TypProjektu;
import pl.com.goldek.unijnyver.model.Wskaznik;
import pl.com.goldek.unijnyver.service.TypProjektuService;
import pl.com.goldek.unijnyver.service.WskaznikService;

import java.util.Set;

@Controller
public class WskaznikController {

    private static Logger logger = LoggerFactory.getLogger(WskaznikController.class);

    @Autowired
    GlobalController globalController;

    @Autowired
    WskaznikService wskaznikService;

    @Autowired
    TypProjektuService typProjektuService;

    @RequestMapping(value = "/wskazniki")
    public @ResponseBody
    Set<Wskaznik> wskaznikiDlaTypuProjektu(
            @RequestParam(value = "typProjektu", required = true) int idTypProjektu) {
        TypProjektu typProjektu = typProjektuService.findById(idTypProjektu);
        logger.info("Szukam wskaźników dla typu projetku: " + typProjektu);
        logger.info(typProjektu.getWskazniki().toString());
        return typProjektu.getWskazniki();
    }

    @RequestMapping(value = "/wskazniki/{id}/", method = RequestMethod.GET)
    public @ResponseBody
    Wskaznik getJednostka(
            @PathVariable("id") int id) {
        logger.info("Get in getJednostka dla id" + id);
        Wskaznik wskaznik = wskaznikService.findById(id);
        return wskaznik;
    }

    @CrossOrigin
    @RequestMapping(value = "/wskazniki/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody
    boolean deleteWskaznik(
            @PathVariable("id") int id) {
        logger.info("Usuwamy id " + id);
        return wskaznikService.delete(id);
    }


}
