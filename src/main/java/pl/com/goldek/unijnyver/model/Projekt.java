package pl.com.goldek.unijnyver.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "projekty")
public class Projekt {

    private int id;
    private Uzytkownik uzytkownik;
    @JsonManagedReference
    private TypProjektu typProjektu;
    private LocalDateTime dataZgloszenia;
    private String nazwa;
    private String adresKorespondencyjny;
    private String adresProjektu;
    private String opis;
    private BigDecimal kosztProjektu;
    private BigDecimal wartoscDofinansowania;
    @JsonManagedReference
    private StatusProjektu statusProjektu;
    @JsonManagedReference
    private Set<WartoscWskaznika> wartosciWskaznikowProjektu = new HashSet<>();

    public Projekt() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    @OneToOne
    @JoinColumn(name = "id_uzytkownika", referencedColumnName = "id")
    public Uzytkownik getUzytkownik() {
        return uzytkownik;
    }

    public void setUzytkownik(Uzytkownik uzytkownik) {
        this.uzytkownik = uzytkownik;
    }

    @Column(name = "data_zgloszenia")
    public LocalDateTime getDataZgloszenia() {
        return dataZgloszenia;
    }

    public void setDataZgloszenia(LocalDateTime dataZgloszenia) {
        this.dataZgloszenia = dataZgloszenia;
    }

    @ManyToOne
    @JoinColumn(name = "typ_projektu")
    public TypProjektu getTypProjektu() {
        return typProjektu;
    }

    public void setTypProjektu(TypProjektu typProjektu) {
        this.typProjektu = typProjektu;
    }

    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Column(name = "adres_korespondencyjny")
    public String getAdresKorespondencyjny() {
        return adresKorespondencyjny;
    }

    public void setAdresKorespondencyjny(String adresKorespondencyjny) {
        this.adresKorespondencyjny = adresKorespondencyjny;
    }

    @Column(name = "adres_projektu")
    public String getAdresProjektu() {
        return adresProjektu;
    }

    public void setAdresProjektu(String adresProjektu) {
        this.adresProjektu = adresProjektu;
    }

    @Column(name = "opis")
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Column(name = "koszt_projektu")
    public BigDecimal getKosztProjektu() {
        return kosztProjektu;
    }

    public void setKosztProjektu(BigDecimal kosztProjektu) {
        this.kosztProjektu = kosztProjektu;
    }

    @Column(name = "wartosc_dofinansowania")
    public BigDecimal getWartoscDofinansowania() {
        return wartoscDofinansowania;
    }

    public void setWartoscDofinansowania(BigDecimal wartoscDofinansowania) {
        this.wartoscDofinansowania = wartoscDofinansowania;
    }

    @ManyToOne
    @JoinColumn(name = "status_projektu")
    public StatusProjektu getStatusProjektu() {
        return statusProjektu;
    }

    public void setStatusProjektu(StatusProjektu statusProjektu) {
        this.statusProjektu = statusProjektu;
    }

    @OneToMany(mappedBy = "projekt")
    public Set<WartoscWskaznika> getWartosciWskaznikowProjektu() {
        return wartosciWskaznikowProjektu;
    }

    public void setWartosciWskaznikowProjektu(Set<WartoscWskaznika> wartosciWskaznikowProjektu) {
        this.wartosciWskaznikowProjektu = wartosciWskaznikowProjektu;
    }

    @Override
    public String toString() {
        return "Projekt{" +
                "id=" + id +
                ", uzytkownik=" + uzytkownik +
                ", typProjektu=" + typProjektu +
                ", nazwa='" + nazwa + '\'' +
                ", adresKorespondencyjny='" + adresKorespondencyjny + '\'' +
                ", adresProjektu='" + adresProjektu + '\'' +
                ", opis='" + opis + '\'' +
                ", kosztProjektu=" + kosztProjektu +
                ", wartoscDofinansowania=" + wartoscDofinansowania +
                ", statusProjektu=" + statusProjektu +
                ", wskaznikiProjektu=" + wartosciWskaznikowProjektu +
                '}';
    }
}
