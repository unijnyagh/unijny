package pl.com.goldek.unijnyver.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "status_projektu")
public class StatusProjektu {

    private int id;
    private String opisStatusu;
    @JsonBackReference
    private Collection<Projekt> projekt = new ArrayList<>();

    public StatusProjektu() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "opis_statusu")
    public String getOpisStatusu() {
        return opisStatusu;
    }

    public void setOpisStatusu(String opisStatusu) {
        this.opisStatusu = opisStatusu;
    }

    @OneToMany(mappedBy = "statusProjektu")
    public Collection<Projekt> getProjekt() {
        return projekt;
    }

    public void setProjekt(Collection<Projekt> projekt) {
        this.projekt = projekt;
    }

    @Override
    public String toString() {
        return "StatusProjektu{" +
                "id=" + id +
                ", opisStatusu='" + opisStatusu + '\'' +
                '}';
    }
}
