package pl.com.goldek.unijnyver.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "typ_projektu")
public class TypProjektu {

    private int id;
    private String nazwa;
    @JsonBackReference
    private Collection<Projekt> projekty = new ArrayList<>();
    @JsonManagedReference
    private Set<Wskaznik> wskazniki = new HashSet<>();

    public TypProjektu() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @OneToMany(mappedBy = "typProjektu")
    public Collection<Projekt> getProjekty() {
        return projekty;
    }

    public void setProjekty(Collection<Projekt> projekty) {
        this.projekty = projekty;
    }

    @ManyToMany
    @JoinTable(name = "typ_wskaznik",
            joinColumns = @JoinColumn(name = "id_typu_projektu"),
            inverseJoinColumns = @JoinColumn(name = "id_wskaznika"))
    public Set<Wskaznik> getWskazniki() {
        return wskazniki;
    }

    public void setWskazniki(Set<Wskaznik> wskazniki) {
        this.wskazniki = wskazniki;
    }

    @Override
    public String toString() {
        return "TypProjektu{" +
                "id=" + id +
                ", nazwa='" + nazwa + '\'' +
//                ", projekty=" + projekty +
                ", wskazniki=" + wskazniki +
                '}';
    }
}
