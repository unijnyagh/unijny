package pl.com.goldek.unijnyver.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "uzytkownicy")
public class Uzytkownik {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    private int id;

    @Column(name = "imie")
    private String imie;

    @Column(name = "nazwisko")
    private String nazwisko;

    @Column(name = "rola")
    private int rola;

    @Column(name = "haslo")
    private String haslo;

    @Transient
    @Column(name = "haslo_2")
    private String haslo_2;

    @Column(name = "data_rejestracji")
    private LocalDateTime dataRejestracji;

    @Column(name = "email")
    private String email;

    @Column(name = "aktywne")
    private boolean czyAktywne;


    public Uzytkownik() {
    }

    public Uzytkownik(String imie, String nazwisko, int rola, String haslo, String email) {
        this.setImie(imie);
        this.setNazwisko(nazwisko);
        this.setRola(rola);
        this.setHaslo(haslo);
        this.setDataRejestracji(LocalDateTime.now());
        this.setEmail(email);
        this.setCzyAktywne(true);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getRola() {
        return rola;
    }

    public void setRola(int rola) {
        this.rola = rola;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public LocalDateTime getDataRejestracji() {
        return dataRejestracji;
    }

    public void setDataRejestracji(LocalDateTime dataRejestracji) {
        this.dataRejestracji = dataRejestracji;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isCzyAktywne() {
        return czyAktywne;
    }

    public void setCzyAktywne(boolean czyAktywne) {
        this.czyAktywne = czyAktywne;
    }

    public String getHaslo_2() {
        return haslo_2;
    }

    public void setHaslo_2(String haslo_2) {
        this.haslo_2 = haslo_2;
    }


    @Override
    public int hashCode() {
        return Objects.hash(id, imie, nazwisko, dataRejestracji, email, haslo, haslo_2, rola);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Uzytkownik u = (Uzytkownik) obj;
        return id == u.id &&
                rola == u.rola &&
                dataRejestracji.equals(u.dataRejestracji) &&
                Objects.equals(imie, u.imie) &&
                Objects.equals(nazwisko, u.nazwisko) &&
                Objects.equals(haslo, u.haslo) &&
                Objects.equals(haslo_2, u.haslo_2) &&
                Objects.equals(email, u.email);
    }

    @Override
    public String toString() {
        return "Uzytkownik{" +
                "id=" + id +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", rola=" + rola +
                ", haslo='" + haslo + '\'' +
                ", dataRejestracji=" + dataRejestracji +
                ", email='" + email + '\'' +
                '}';
    }
}
