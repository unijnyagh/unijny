package pl.com.goldek.unijnyver.model;

import javax.persistence.*;

@Entity
@Table(name = "wartosc_wskaznika")
public class WartoscWskaznika {

    @EmbeddedId
    WartoscWskaznikaKey id;

    @ManyToOne
    @MapsId("idWskaznika")
    @JoinColumn(name = "id_wskaznika")
    Wskaznik wskaznik;

    @ManyToOne
    @MapsId("idProjektu")
    @JoinColumn(name = "id_projektu")
    Projekt projekt;

    @Column(name = "wartosc")
    double wartoscWskaznika;

    public WartoscWskaznika() {
    }

    public WartoscWskaznika(Wskaznik wskaznik, Projekt projekt, double wartoscWskaznika) {
        this.wskaznik = wskaznik;
        this.projekt = projekt;
        this.wartoscWskaznika = wartoscWskaznika;
        this.id = new WartoscWskaznikaKey(projekt.getId(), wskaznik.getId());
    }

    public WartoscWskaznikaKey getId() {
        return id;
    }

    public void setId(WartoscWskaznikaKey id) {
        this.id = id;
    }


    public Wskaznik getWskaznik() {
        return wskaznik;
    }

    public void setWskaznik(Wskaznik wskaznik) {
        this.wskaznik = wskaznik;
    }


    public Projekt getProjekt() {
        return projekt;
    }

    public void setProjekt(Projekt projekt) {
        this.projekt = projekt;
    }

    public double getWartoscWskaznika() {
        return wartoscWskaznika;
    }

    public void setWartoscWskaznika(double wartoscWskaznika) {
        this.wartoscWskaznika = wartoscWskaznika;
    }

    @Override
    public String toString() {
        return "WartoscWskaznika{" +
                "id=" + id +
                ", wskaznik nazwa=" + wskaznik.getNazwa() +
                ", projekt id=" + projekt.getId() +
                ", wartoscWskaznika=" + wartoscWskaznika +
                '}';
    }
}
