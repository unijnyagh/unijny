package pl.com.goldek.unijnyver.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class WartoscWskaznikaKey implements Serializable {

    @Column(name = "id_projektu")
    int idProjektu;

    @Column(name = "id_wskaznika")
    int idWskaznika;

    public WartoscWskaznikaKey() {
    }

    public WartoscWskaznikaKey(int idProjektu, int idWskaznika) {
        this.idProjektu = idProjektu;
        this.idWskaznika = idWskaznika;
        System.out.println("WARTOSC WSKAZNIKA KEY Konstruktor id Pr :" + idProjektu);
    }

    public int getIdProjektu() {
        return idProjektu;
    }

    public void setIdProjektu(int idProjektu) {
        this.idProjektu = idProjektu;
    }

    public int getIdWskaznika() {
        return idWskaznika;
    }

    public void setIdWskaznika(int idWskaznika) {
        this.idWskaznika = idWskaznika;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WartoscWskaznikaKey that = (WartoscWskaznikaKey) o;
        return idProjektu == that.idProjektu &&
                idWskaznika == that.idWskaznika;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProjektu, idWskaznika);
    }
}
