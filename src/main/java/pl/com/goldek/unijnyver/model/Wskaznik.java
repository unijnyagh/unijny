package pl.com.goldek.unijnyver.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "wskaznik")
public class Wskaznik implements Serializable {

    private int id;
    private String nazwa;
    private String jednostkaMiary;
    @JsonBackReference
    private Set<WartoscWskaznika> projekty = new HashSet<>();
    @JsonBackReference
    private Set<TypProjektu> typyProjektu = new HashSet<>();

    public Wskaznik() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Column(name = "jednostka_miary")
    public String getJednostkaMiary() {
        return jednostkaMiary;
    }

    public void setJednostkaMiary(String jednostkaMiary) {
        this.jednostkaMiary = jednostkaMiary;
    }

    @ManyToMany(mappedBy = "wskazniki")
    public Set<TypProjektu> getTypyProjektu() {
        return typyProjektu;
    }

    public void setTypyProjektu(Set<TypProjektu> typyProjektu) {
        this.typyProjektu = typyProjektu;
    }

    @OneToMany(mappedBy = "wskaznik")
    public Set<WartoscWskaznika> getProjekty() {
        return projekty;
    }

    public void setProjekty(Set<WartoscWskaznika> projekty) {
        this.projekty = projekty;
    }

    @Override
    public String toString() {
        return "Wskaznik{" +
                "id=" + id +
                ", nazwa='" + nazwa + '\'' +
                ", jednostkaMiary='" + jednostkaMiary + '\'' +
//                ", projekty=" + projekty +
//                ", typyProjektu=" + typyProjektu +
                '}';
    }
}
