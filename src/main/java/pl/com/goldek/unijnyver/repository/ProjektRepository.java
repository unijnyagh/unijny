package pl.com.goldek.unijnyver.repository;

import org.springframework.data.repository.CrudRepository;
import pl.com.goldek.unijnyver.model.Projekt;
import pl.com.goldek.unijnyver.model.Uzytkownik;

import java.util.Collection;

public interface ProjektRepository extends CrudRepository<Projekt, Integer> {

    Collection<Projekt> findAllByUzytkownik(Uzytkownik uzytkownik);

}
