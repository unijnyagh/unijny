package pl.com.goldek.unijnyver.repository;

import org.springframework.data.repository.CrudRepository;
import pl.com.goldek.unijnyver.model.StatusProjektu;

public interface StatusProjektuRepository extends CrudRepository<StatusProjektu, Integer> {


}
