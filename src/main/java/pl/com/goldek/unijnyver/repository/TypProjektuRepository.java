package pl.com.goldek.unijnyver.repository;

import org.springframework.data.repository.CrudRepository;
import pl.com.goldek.unijnyver.model.TypProjektu;
import pl.com.goldek.unijnyver.model.Wskaznik;

import java.util.Set;

public interface TypProjektuRepository extends CrudRepository<TypProjektu, Integer> {

    Set<Wskaznik> findWskaznikById(int idTypuWskaznika);
}
