package pl.com.goldek.unijnyver.repository;

import org.springframework.data.repository.CrudRepository;
import pl.com.goldek.unijnyver.model.Uzytkownik;

public interface UzytkownikRepository extends CrudRepository<Uzytkownik, Integer> {

    Uzytkownik findUzytkownikByEmail(String email);

    Uzytkownik findUzytkownikByImieAndNazwisko(String imie, String nazwisko);
}
