package pl.com.goldek.unijnyver.repository;

import org.springframework.data.repository.CrudRepository;
import pl.com.goldek.unijnyver.model.WartoscWskaznika;

public interface WartoscWskaznikaRepository extends CrudRepository<WartoscWskaznika, Integer> {

}
