package pl.com.goldek.unijnyver.repository;

import org.springframework.data.repository.CrudRepository;
import pl.com.goldek.unijnyver.model.Wskaznik;


public interface WskaznikRepository extends CrudRepository<Wskaznik, Integer> {

    Wskaznik findWskaznikById(int id);

}
