package pl.com.goldek.unijnyver.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.com.goldek.unijnyver.model.Uzytkownik;
import pl.com.goldek.unijnyver.service.UzytkownikService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class AuthUserDetailsService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(AuthUserDetailsService.class);

    @Autowired
    private UzytkownikService uzytkownikService;

    private org.springframework.security.core.userdetails.User springUser;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        Uzytkownik uzytkownik = getUzytkownikDetails(email);
        if (uzytkownik != null) {
            springUser = new org.springframework.security.core.userdetails.User(uzytkownik.getEmail(),
                    uzytkownik.getHaslo(),
                    enabled,
                    accountNonExpired,
                    credentialsNonExpired,
                    accountNonLocked,
                    getAuthorities(uzytkownik.getRola())
            );
            return springUser;
        } else {
            springUser = new org.springframework.security.core.userdetails.User("empty",
                    "empty",
                    false,
                    true,
                    true,
                    false,
                    getAuthorities(1)
            );
            return springUser;
        }


    }

    private Collection<? extends GrantedAuthority> getAuthorities(int rola) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        if (rola == 1) {
            authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        } else if (rola == 2) {
            authList.add(new SimpleGrantedAuthority("ROLE_USER"));
        }

        return authList;
    }

    private Uzytkownik getUzytkownikDetails(String email) {

        Uzytkownik uzytkownik = uzytkownikService.findByEmail(email);
        if (uzytkownik == null) {
            logger.warn("uzytkownik '" + email + "' on null");
        } else {
            logger.info(uzytkownik.toString());
        }
        return uzytkownik;
    }

}
