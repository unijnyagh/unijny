package pl.com.goldek.unijnyver.service;

import pl.com.goldek.unijnyver.model.Projekt;
import pl.com.goldek.unijnyver.model.Uzytkownik;
import pl.com.goldek.unijnyver.model.Wskaznik;

import java.util.Collection;

public interface ProjektService {

    Projekt save(Projekt projekt);

    Boolean delete(int id);

    Projekt update(Projekt projekt);

    Projekt findById(int id);

    Collection<Projekt> findAll();

    Collection<Projekt> findAllByUzytkownik(Uzytkownik uzytkownik);

}
