package pl.com.goldek.unijnyver.service;

import jdk.net.SocketFlow;
import pl.com.goldek.unijnyver.model.StatusProjektu;

import java.util.Collection;

public interface StatusProjektuService {

    StatusProjektu save(StatusProjektu statusProjektu);

    StatusProjektu update(StatusProjektu statusProjektu);

    Boolean delete(int id);

    StatusProjektu findById (int id);

    Collection<StatusProjektu> findAll();


}
