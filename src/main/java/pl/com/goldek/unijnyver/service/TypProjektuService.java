package pl.com.goldek.unijnyver.service;

import pl.com.goldek.unijnyver.model.TypProjektu;
import pl.com.goldek.unijnyver.model.Uzytkownik;
import pl.com.goldek.unijnyver.model.Wskaznik;

import javax.persistence.CollectionTable;
import java.util.Collection;

public interface TypProjektuService {
    
    TypProjektu save(TypProjektu typProjektu);

    Boolean delete(int id);

    TypProjektu update(TypProjektu typProjektu);

    TypProjektu findById(int id);

    Collection<TypProjektu> findAll();

    Collection<Wskaznik> findWskaznikiByProjektId(int idTypuProjektu);


}
