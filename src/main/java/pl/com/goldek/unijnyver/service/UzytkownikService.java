package pl.com.goldek.unijnyver.service;

import pl.com.goldek.unijnyver.model.Uzytkownik;

import java.util.Collection;
import java.util.Collections;

public interface UzytkownikService {

    Uzytkownik save(Uzytkownik uzytkownik);

    Boolean delete(int id);

    Uzytkownik update(Uzytkownik uzytkownik);

    Uzytkownik findById(int id);

    Uzytkownik findByImieAndNazwisko(String imie, String nazwisko);

    Uzytkownik findByEmail(String email);

    Collection<Uzytkownik> findAll();

}
