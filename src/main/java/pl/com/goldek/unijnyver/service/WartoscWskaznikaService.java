package pl.com.goldek.unijnyver.service;

import pl.com.goldek.unijnyver.model.WartoscWskaznika;

import java.util.Collection;
import java.util.Set;

public interface WartoscWskaznikaService {

    WartoscWskaznika save(WartoscWskaznika wartoscWskaznika);

    Boolean delete(int id);

    WartoscWskaznika update(WartoscWskaznika wartoscWskaznika);

    WartoscWskaznika findById(int id);

    Collection<WartoscWskaznika> findAll();

    Set<WartoscWskaznika> saveCollection(Set<WartoscWskaznika> wartosciWskaznika);
}
