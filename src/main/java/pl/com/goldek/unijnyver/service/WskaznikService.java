package pl.com.goldek.unijnyver.service;

import pl.com.goldek.unijnyver.model.Uzytkownik;
import pl.com.goldek.unijnyver.model.Wskaznik;

import java.util.Collection;

public interface WskaznikService {

    Wskaznik save(Wskaznik wskaznik);

    Boolean delete(int id);

    Wskaznik update(Wskaznik wskaznik);

    Wskaznik findById(int id);

    Collection<Wskaznik> findAll();

}
