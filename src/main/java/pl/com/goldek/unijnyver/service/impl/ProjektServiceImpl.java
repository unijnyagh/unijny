package pl.com.goldek.unijnyver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.goldek.unijnyver.model.Projekt;
import pl.com.goldek.unijnyver.model.Uzytkownik;
import pl.com.goldek.unijnyver.model.Wskaznik;
import pl.com.goldek.unijnyver.repository.ProjektRepository;
import pl.com.goldek.unijnyver.service.ProjektService;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class ProjektServiceImpl implements ProjektService {

    @Autowired
    private ProjektRepository projektRepository;

    @Override
    public Projekt save(Projekt projekt) {
        return projektRepository.save(projekt);
    }

    @Override
    public Boolean delete(int id) {
        if (projektRepository.existsById(id)){
            projektRepository.deleteById(id);
            return true;
        }
        else return false;
    }

    @Override
    public Projekt update(Projekt projekt) {
        return projektRepository.save(projekt);
    }

    @Override
    public Projekt findById(int id) {
        return projektRepository.findById(id).get();
    }

    @Override
    public Collection<Projekt> findAll() {
        Iterable<Projekt> itr = projektRepository.findAll();
        return (Collection<Projekt>) itr;
    }

    @Override
    public Collection<Projekt> findAllByUzytkownik(Uzytkownik uzytkownik) {
        Iterable<Projekt> itr =  projektRepository.findAllByUzytkownik(uzytkownik);
        return (Collection<Projekt>) itr;
    }
}
