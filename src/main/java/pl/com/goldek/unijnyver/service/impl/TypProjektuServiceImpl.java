package pl.com.goldek.unijnyver.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.goldek.unijnyver.model.TypProjektu;
import pl.com.goldek.unijnyver.model.Wskaznik;
import pl.com.goldek.unijnyver.repository.TypProjektuRepository;
import pl.com.goldek.unijnyver.service.TypProjektuService;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class TypProjektuServiceImpl implements TypProjektuService {

    private static final Logger logger = LoggerFactory.getLogger(TypProjektuServiceImpl.class);

    @Autowired
    private TypProjektuRepository typProjektuRepository;

    @Override
    public TypProjektu save(TypProjektu typProjektu) {
        return typProjektuRepository.save(typProjektu);
    }

    @Override
    public Boolean delete(int id) {
        if (typProjektuRepository.existsById(id)){
            typProjektuRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public TypProjektu update(TypProjektu typProjektu) {
        return typProjektuRepository.save(typProjektu);
    }

    @Override
    public TypProjektu findById(int id) {
        logger.debug("Szukam po ID, dla id: "+id );
        return typProjektuRepository.findById(id).get();
    }

    @Override
    public Collection<TypProjektu> findAll() {
        Iterable<TypProjektu> itr = typProjektuRepository.findAll();
        return (Collection<TypProjektu>) itr;
    }

    @Override
    public Collection<Wskaznik> findWskaznikiByProjektId(int id) {
        Iterable<Wskaznik> itr = typProjektuRepository.findById(id).get().getWskazniki();
        return (Collection<Wskaznik>) itr;
    }
}
