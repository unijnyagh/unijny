package pl.com.goldek.unijnyver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.goldek.unijnyver.model.Uzytkownik;
import pl.com.goldek.unijnyver.repository.UzytkownikRepository;
import pl.com.goldek.unijnyver.service.UzytkownikService;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class UzytkownikServiceImpl implements UzytkownikService {

    @Autowired
    private UzytkownikRepository uzytkownikRepository;

    @Override
    public Uzytkownik save(Uzytkownik uzytkownik) {
        return uzytkownikRepository.save(uzytkownik);
    }

    @Override
    public Boolean delete(int id) {
        if(uzytkownikRepository.existsById(id)) {
            uzytkownikRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public Uzytkownik update(Uzytkownik uzytkownik) {
        return uzytkownikRepository.save(uzytkownik);
    }

    @Override
    public Uzytkownik findById(int id) {
        return uzytkownikRepository.findById(id).get();
    }

    @Override
    public Uzytkownik findByImieAndNazwisko(String imie, String nazwisko) {
        return uzytkownikRepository.findUzytkownikByImieAndNazwisko(imie, nazwisko);
    }

    @Override
    public Uzytkownik findByEmail(String email) {
        return uzytkownikRepository.findUzytkownikByEmail(email);
    }


    @Override
    public Collection<Uzytkownik> findAll() {
        Iterable<Uzytkownik> itr = uzytkownikRepository.findAll();
        return (Collection<Uzytkownik>) itr;
    }
}
