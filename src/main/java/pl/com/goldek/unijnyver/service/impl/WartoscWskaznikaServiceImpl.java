package pl.com.goldek.unijnyver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.goldek.unijnyver.model.WartoscWskaznika;
import pl.com.goldek.unijnyver.repository.WartoscWskaznikaRepository;
import pl.com.goldek.unijnyver.service.WartoscWskaznikaService;

import java.util.Collection;
import java.util.Set;

@Service
public class WartoscWskaznikaServiceImpl implements WartoscWskaznikaService {

    @Autowired
    WartoscWskaznikaRepository wartoscWskaznikaRepository;

    @Override
    public WartoscWskaznika save(WartoscWskaznika wartoscWskaznika) {
        System.out.println("WartoscWskaznikaServicIMPL.class ---> zapisujemy: Id projektu"
                + wartoscWskaznika.getProjekt().getId() + " teraz nazwa projektu"
                + wartoscWskaznika.getProjekt().getNazwa() + " teraz nazwa wska"
                + wartoscWskaznika.getWskaznik().getNazwa()
                + "  " + wartoscWskaznika.getWartoscWskaznika());
        return wartoscWskaznikaRepository.save(wartoscWskaznika);
    }

    @Override
    public Boolean delete(int id) {
        if (wartoscWskaznikaRepository.existsById(id)){
            wartoscWskaznikaRepository.deleteById(id);
            return true;
        }
        return false;

    }

    @Override
    public WartoscWskaznika update(WartoscWskaznika wartoscWskaznika) {
        return wartoscWskaznikaRepository.save(wartoscWskaznika);
    }

    @Override
    public WartoscWskaznika findById(int id) {
        return wartoscWskaznikaRepository.findById(id).get();
    }

    @Override
    public Collection<WartoscWskaznika> findAll() {
        Iterable<WartoscWskaznika> wartosciWskaznikow = wartoscWskaznikaRepository.findAll();
        return (Collection<WartoscWskaznika>) wartosciWskaznikow;
    }

    @Override
    public Set<WartoscWskaznika> saveCollection(Set<WartoscWskaznika> wartosciWskaznika) {
        for (WartoscWskaznika wsk : wartosciWskaznika){
            System.out.println("WartoscWskaznikaServiceIMPL.saveCollection: "
                            + wsk.getProjekt().getId() + " WARTOść "+ wsk.getWskaznik().getNazwa() + ", wartość  " + wsk.getWartoscWskaznika() );
            wartoscWskaznikaRepository.save(wsk);
        }
        return wartosciWskaznika;
    }
}
