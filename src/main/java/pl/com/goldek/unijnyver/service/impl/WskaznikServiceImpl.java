package pl.com.goldek.unijnyver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.goldek.unijnyver.model.Wskaznik;
import pl.com.goldek.unijnyver.repository.WskaznikRepository;
import pl.com.goldek.unijnyver.service.WskaznikService;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

@Service
public class WskaznikServiceImpl implements WskaznikService {

    @Autowired
    private WskaznikRepository wskaznikRepository;

    @Override
    public Wskaznik save(Wskaznik wskaznik) {
        return wskaznikRepository.save(wskaznik);
    }

    @Override
    public Boolean delete(int id) {
        if (wskaznikRepository.existsById(id)){
            wskaznikRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public Wskaznik update(Wskaznik wskaznik) {
        return wskaznikRepository.save(wskaznik);
    }

    @Override
    public Wskaznik findById(int id) {
        return wskaznikRepository.findWskaznikById(id);
    }


    @Override
    public Collection<Wskaznik> findAll() {
        Iterable<Wskaznik> itr = wskaznikRepository.findAll();
        return (Collection<Wskaznik>) itr;
    }

}
