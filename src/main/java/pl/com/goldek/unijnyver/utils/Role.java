package pl.com.goldek.unijnyver.utils;

public enum Role {
    ROLE_ADMIN(1), ROLE_USER(2);
    private int value;

    Role(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
