package pl.com.goldek.unijnyver.utils;

import pl.com.goldek.unijnyver.model.WartoscWskaznika;

import java.util.HashSet;
import java.util.Set;

public class WartoscWskaznikSetWrapper {

    private Set<WartoscWskaznika> wartoscWskaznikow = new HashSet<>();

    public Set<WartoscWskaznika> getWartoscWskaznikow() {
        return wartoscWskaznikow;
    }

    public void setWartoscWskaznikow(Set<WartoscWskaznika> wartoscWskaznikow) {
        this.wartoscWskaznikow = wartoscWskaznikow;
    }
}
