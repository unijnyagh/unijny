CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `status_projektu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `status_projektu` (
  `id_statusu_projektu` INT NOT NULL,
  `opis_statusu` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id_statusu_projektu`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `typ_projektu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `typ_projektu` (
  `id_typu_projektu` INT NOT NULL,
  `nazwa` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id_typu_projektu`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wartoscWskaznika`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wartoscWskaznika` (
  `id_projektu` INT NOT NULL,
  `typ` INT NOT NULL,
  `nazwa` VARCHAR(100) NOT NULL,
  `adres_korespondencyjny` VARCHAR(200) NOT NULL,
  `adres_projektu` VARCHAR(200) NOT NULL,
  `opis` VARCHAR(1000) NOT NULL,
  `koszt_projektu` DECIMAL(9,2) NOT NULL,
  `wartosc_dofinansowania` DECIMAL(9,2) NOT NULL,
  `status_projektu` INT NOT NULL,
  `status_projektu_id_status_projektu` INT NOT NULL,
  `typ_projektu_id_typ_projektu` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Projekty_StatusProjektu1`
    FOREIGN KEY (`status_projektu_id_status_projektu`)
    REFERENCES `status_projektu` (`id_statusu_projektu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Projekty_TypProjektu1`
    FOREIGN KEY (`typ_projektu_id_typ_projektu`)
    REFERENCES `typ_projektu` (`id_typu_projektu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `uzytkownicy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `uzytkownicy` (
  `id_uzytkownika` INT NOT NULL,
  `imie` VARCHAR(20) NOT NULL,
  `nazwisko` VARCHAR(50) NOT NULL,
  `data_rejestracji` DATETIME NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `rola` TINYINT NOT NULL,
  `projekty_id_projektu` INT NOT NULL,
  PRIMARY KEY (`id_uzytkownika`),
  CONSTRAINT `fk_Użytkownicy_Projekty`
    FOREIGN KEY (`projekty_id_projektu`)
    REFERENCES `wartoscWskaznika` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `wskaznik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wskaznik` (
  `id_wskaznika` INT NOT NULL,
  `nazwa` VARCHAR(50) NOT NULL,
  `jednostka_miary` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`id_wskaznika`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wartosc_wskaznika`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wartosc_wskaznika` (
  `wartosc` DOUBLE NOT NULL,
  `projekty_id_projektu` INT NOT NULL,
  `wskaznik_id_wskaznika` INT NOT NULL,
  CONSTRAINT `fk_WartoscWskaznika_Projekty1`
    FOREIGN KEY (`projekty_id_projektu`)
    REFERENCES `wartoscWskaznika` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_WartoscWskaznika_Wskaznik1`
    FOREIGN KEY (`wskaznik_id_wskaznika`)
    REFERENCES `wskaznik` (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `typ_wskaznik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `typ_wskaznik` (
  `wskaznik_id_wskaznika` INT NOT NULL,
  `typ_projektu_id_typu_projektu` INT NOT NULL,
  PRIMARY KEY (`wskaznik_id_wskaznika`, `typ_projektu_id_typu_projektu`),
  CONSTRAINT `fk_TypWskaznik_Wskaznik1`
    FOREIGN KEY (`wskaznik_id_wskaznika`)
    REFERENCES `wskaznik` (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TypWskaznik_TypProjektu1`
    FOREIGN KEY (`typ_projektu_id_typu_projektu`)
    REFERENCES `typ_projektu` (`id_typu_projektu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
